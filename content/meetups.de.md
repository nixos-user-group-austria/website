---
title: "Treffen"
layout: "meetups"
summary: "Vergangene und zukünftige Treffen"
description: "Treffen werden sowohl on- als auch offline in Graz und Wien stattfinden.

Sobald wir einige regelmäßige Teilnehmer haben, werde ich alle Treffen auf einer Mobilizon-Instanz ankündigen, damit jeder, der möchte, teilnehmen kann. Die Treffen werden in englischer Sprache auch auf NixOS Discourse angekündigt.

Zur Terminkoordination wird in Zukunft vor den Treffen entweder eine Umfrage im Matrix-Channel gestartet, oder ein Link zu einer Framadate-Instanz gepostet werden."
---
