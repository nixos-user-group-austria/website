---
title: "Topics"
description: "List of topics discussed at user group meetups, with links to respective project pages, repositories or relevant learning material."
ShowReadingTime: false

# 'Under Construction' cover image
cover:
  image: "/images/nix-under-construction.svg"
  caption: "This page is still under construction."
  alt: "Nix snowflake logo with a stick figure construction worker shovelling, placed in the middle of the snowflake"
  hidden: false
---


- [NixOS](https://nixos.org/)
- [Nix Documentation](https://nix.dev/)
- [Home Manager](https://nix-community.github.io/home-manager/index.html)
