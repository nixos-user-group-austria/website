---
title: "Meetups"
layout: "meetups"
summary: "Past and future meetups"
description: "Meetups will be held both on- and offline in Graz and Vienna.

As soon as we have some regular participants, I will schedule all meetups using a Mobilizon instance, so that everyone who wants to can join. Meetups will also be announced on NixOS Discourse.

In the future, to schedule meetups, either a poll will be started in the Matrix channel, or a link to a Framadate instance will be posted."
---
