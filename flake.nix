{
  description = "Website of NixOS User Group Austria";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";

    # Import theme
    papermod = {
      url = "git+https://codeberg.org/nixos-user-group-austria/hugo-papermod-theme?ref=main";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, papermod }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        nugaustria = pkgs.stdenv.mkDerivation {
          name = "nugaustria";
          # Exclude themes and public folder from build sources
          src = builtins.filterSource
            (path: type: !(type == "directory" &&
              (baseNameOf path == "themes" ||
                baseNameOf path == "public")))
            ./.;
          # Link theme to themes folder and build
          buildPhase = ''
            mkdir -p themes
            ln -sf ${papermod} themes/papermod
            ${pkgs.hugo}/bin/hugo --minify
          '';
          installPhase = ''
            cp -r public $out
          '';
          meta = with pkgs.lib; {
            description = "Website of NixOS User Group Austria";
            platforms = platforms.all;
          };
        };
      in
      {
        packages = {
          nugaustria = nugaustria;
          default = nugaustria;
        };

        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [ hugo ];
          # Link theme to themes folder
          shellHook = ''
            mkdir -p themes
            ln -sf ${papermod} themes/papermod
          '';
        };
      });
}
