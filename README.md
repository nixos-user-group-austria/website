# NixOS User Group Austria Website [<img src="static/images/logo.svg" align="right" width="28">](https://nixos.at)

This is the repository for the user group's website template.

The website is generated from this template using the static site generator framework [HUGO](https://gohugo.io) and is currently hosted by [Easyname](https://easyname.at).

It is accessible at https://nixos.at.


## Building locally

With [direnv](https://direnv.net/) installed, building the website locally should be as easy as checking out the repository, navigating to the directory and running the following:

```
direnv allow .
nix build
```

To run the Hugo server and check the resulting website, simply run `hugo server` or `hugo server -d` to include drafts as well.
